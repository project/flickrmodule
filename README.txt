WARNING:
========
This module is EARLY BETA. It is unpolished, has no help text, and does
not properly catch some errors. Use at your own risk.

Installing:
a) Get an API key from Flickr (http://www.flickr.com/services/api/misc.api_keys.html)
b) Get your NSID from flickr (go to your photos page, view source, find global_nsid, note value)
c) Go to http://www.jluster.org/admin/settings/flickr and configure your setup.
d) In your CSS file, create a class named whatever you like. Use .flickr for clarity. Make it look the way you want your images to look.
   Example:
   .flickr {
    border: 2px solid #333;
    padding: 4px;
    margin: 6px;
   }
e) create a directory somewhere your web server has access to, make this directory rw for the server
f) enter the directory path into "Cache Location:" on the setup page
g) Enjoy
z) Send an email to jluster@jluster.org and tell me you're testing the module. See Jonas' beggings for petty cash for the
   "let Jonas sleep" hotel fund. Donate. Make me happy.

WHAT IT DOES:
=============
If added as an input filter, allows you to add images from your flickr account to your postings like so: {{<tag>:<photo_id>}}, 
where <tag> can be:

flickr and flickr_m - get a "medium" size image from Flickr
flickrnl - same as flickr, but this time without a link back. Make sure to set one yourself, or you violate the AUP
flickrtn and flickr_s - a thumbnail (75x75)
flickr_t - 100x sized image
flickrbig - 500x sized image

If installed, a new page, called /flickr will show all your photosets, as well. This page can be themed. In your theme file add a function called <themename>_flickr_photoset. See theme_flickr_photoset inside the module for an example.

WHAT'S NEXT:
============

a) Per registered user flickrsets
  a/1) FotoBuzz support
b) more functions
c) moblogs and daily-picture setups
d) random image
e) more coolness
f) uploads from Drupal